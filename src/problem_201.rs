/*!
 Problem 201
 Feb 21, 2024

 Given two integers `left` and `right`, return the bitwise and of all
 numbers in the range `[left, right]` (inclusive)
 */

pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn range_bitwise_and(left: i32, right: i32) -> i32 {
        assert!(left >= 0);
        assert!(right >= 0);

        // Find the most significant bit where they differ. Then, the bitwise
        // AND will keep all the original bits to the left of that point. Everything
        // including and after the first bit where the differ will be a zero
        for i in (0..31).rev() { // Skip first bit, which is for negatives
            let mask = 0b1 << i;

            if (left & mask) != (right & mask) { 
                return (left >> (i+1)) << (i+1);
            }
        }

        left // If we got through the whole loop, then it must be that left == right
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_eq() {
        for i in 0..100 {
            assert_eq!(Solution::range_bitwise_and(i, i), i);
        }
    }

    fn and_range_manually(left: i32, right: i32) -> i32 {
        let mut acc = left;
        for i in left..=right {
            acc &= i
        }
        acc
    }

    #[test]
    fn test_general() {
        assert_eq!(Solution::range_bitwise_and(0b1000, 0b1100), 
            and_range_manually(0b1000, 0b1100));

        assert_eq!(Solution::range_bitwise_and(0b11001000, 0b11010000), 
            and_range_manually(0b11001000, 0b11010000));
    }

    #[test]
    fn test_full_range() {
        assert_eq!(Solution::range_bitwise_and(0, i32::MAX), 
            and_range_manually(0, i32::MAX));
    }
}