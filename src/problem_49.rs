/*!
 Feb 7, 2024

 Given a `Vec<String>`, group all strings that are anagrams of each other.

 For example, `["abc", "cba", "aab"] => [["abc", "cba"], ["aab"]]`
*/

#[allow(dead_code)]
pub struct Solution {}

use std::collections::HashMap;
impl Solution {
    #[allow(dead_code)]
    pub fn group_anagrams(strs: Vec<String>) -> Vec<Vec<String>> {
        let mut counts: HashMap<String, Vec<String>> = HashMap::new();

        for s in strs.iter() {
            let mut sorted_vec = Vec::from(s.clone());
            sorted_vec.sort();
            let sorted_string = String::from_utf8(sorted_vec).expect("Error!");
            
            if let Some(v) = counts.get_mut(&sorted_string) {
                v.push(s.clone());
            } else {
                counts.insert(sorted_string, vec![s.clone()]);
            }
        }

        counts.into_iter().map(|(_, v)| v).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_input() {
        let input: Vec<String> = vec![];
        assert_eq!(Solution::group_anagrams(input).len(), 0);
    }

    #[test]
    fn all_anagrams() {
        let input = vec![String::from("abc"), String::from("cba")];
        assert_eq!(Solution::group_anagrams(input), vec![vec![String::from("abc"), String::from("cba")]])
    }

    #[test]
    fn two_different() {
        let input = vec![String::from("abc"), String::from("baa")];
        let result = Solution::group_anagrams(input);
        assert!(result.contains(&vec![String::from("abc")]));
        assert!(result.contains(&vec![String::from("baa")]));
    }
}