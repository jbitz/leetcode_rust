/*!
 Given an integer `n`, return `true` if it is a power of two, else `false`
 */

pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn is_power_of_two(n: i32) -> bool {
        return n > 0 && (n & (n-1)) == 0;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_up_to_1024() {
        let powers = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024];

        for i in -1023..=1024 {
            assert_eq!(Solution::is_power_of_two(i), powers.contains(&i));
        }
    }
}