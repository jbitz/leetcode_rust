/*!
 Feb 8, 2024

 Given a number `n`, return the least number of perfect squares which can
 be used to sum to `n` (repeats allowed)
*/

use std::collections::HashMap;

pub struct Solution {}

impl Solution {
    fn num_squares_helper(n: i32, cache: &mut HashMap<i32, i32>) -> i32 {
        if n <= 0 {
            return 0;
        }

        if let Some(res) = cache.get(&n) {
            return *res
        }

        let mut best = 0;

        for sq in (1..).map(|x| x * x).take_while(|x| *x <= n) {
            let sub = Solution::num_squares_helper(n - sq, cache) + 1;
            if best == 0 || sub < best {
                best = sub;
            }
        }
        cache.insert(n, best);
        best
    }

    #[allow(dead_code)]
    pub fn num_squares(n: i32) -> i32 {
        let mut cache = HashMap::new();
        Solution::num_squares_helper(n, &mut cache)
    }
}

#[cfg(test)]
mod tests {
    use crate::problem_279::Solution;

    #[test]
    fn test_12() {
        assert_eq!(3, Solution::num_squares(12));
    }

    #[test]
    fn test_13() {
        assert_eq!(2, Solution::num_squares(13));
    }
}