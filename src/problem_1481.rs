/*!
 Problem 1481
 February 16, 2024

 Given a `Vec<i32>` and an integer `k`, return the *least number of unique integers*
 that could be present in the vector after removing exactly `k` elements.
 */

pub struct Solution {}

use std::collections::HashMap;

impl Solution {
    pub fn find_least_num_of_unique_ints(arr: Vec<i32>, k: i32) -> i32 {
        let mut counts = HashMap::new();
        arr.into_iter().for_each(|val| {
            if let Some(c) = counts.get(&val) {
                counts.insert(val, c + 1);
            } else {
                counts.insert(val, 1);
            }
        });

        let mut pairs: Vec<(i32, i32)> = counts.into_iter().collect();
        pairs.sort_by(|a, b| a.1.cmp(&b.1) );

        let mut remaining = k;
        let mut num_removed = 0;
        for i in 0..pairs.len() {
            if pairs[i].1 <= remaining {
                remaining -= pairs[i].1;
                num_removed += 1;
            } else {
                break;
            }
        }

        (pairs.len() - num_removed) as i32
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_k_is_1() {
        let input = vec![3,3,4];
        assert_eq!(Solution::find_least_num_of_unique_ints(input, 1), 1);
    }

    #[test]
    fn test_empty() {
        let input = vec![];
        assert_eq!(Solution::find_least_num_of_unique_ints(input, 0), 0);
    }

    #[test]
    fn test_partial_remove() {
        let input = vec![4,3,1,1,3,3,2];
        assert_eq!(Solution::find_least_num_of_unique_ints(input, 3), 2);
    }
}