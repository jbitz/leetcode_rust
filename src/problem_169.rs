/*!
 Problem 169
 Feb 12, 2024

 Given a `Vec<i32>`, of length `n` find the majority element in the list, that is, the
 element which occurs more than `floor(n / 2)` times. Such an element is guaranteed
 to exist
 */

// Done in Python, because rust doesn't have good random functions without an extra crate
/*
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        from random import randint

        while True:
            target = randint(0, len(nums) - 1)
            count = nums.count(nums[target])
            if count > len(nums) // 2:
                return nums[target]
 */