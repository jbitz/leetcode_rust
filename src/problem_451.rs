/*!
 Feb 7, 2024

 Given a `String` `s`, return a new string which has all the characters
 of `s` sorted by order of frequency.

 For example `"ababcccc" => "ccccaabb"`
*/

#[allow(dead_code)]
pub struct Solution {}

use std::collections::HashMap;
use std::cmp::Ordering;

impl Solution {
    fn build_freq_map(s: &String) -> HashMap<char, usize> {
        let mut counts = HashMap::new();
        s.chars().for_each(|c| {
            if let Some(count) = counts.get(&c) {
                counts.insert(c, count + 1);
            } else {
                counts.insert(c, 1);
            }
        });
        counts
    }

    #[allow(dead_code)]
    pub fn frequency_sort(s: String) -> String {
        let counts = Solution::build_freq_map(&s);
        
        let mut char_vec: Vec<char> = s.chars().collect();
        char_vec.sort_by(|a, b| {
            let result = counts.get(a).cmp(&counts.get(b));
            match result {
                Ordering::Equal => b.cmp(&a),
                _ => result.reverse(),
            }
        });
        let s: String = char_vec.into_iter().collect();
        s
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_string() {
        assert_eq!(String::from(""), Solution::frequency_sort(String::from("")));
    }

    #[test]
    fn all_one_letter() {
        assert_eq!(String::from("aaaa"), Solution::frequency_sort(String::from("aaaa")));
    }

    #[test]
    fn input_ccbabacc() {
        assert_eq!(String::from("ccccbbaa"), Solution::frequency_sort(String::from("ccbabacc")));
    }
}