/*!
Feb 6, 2024
Problem 387: Given a string s, return the *index* of the first unique
character in that string. Return -1 if there are no unique characters.

i.e. `"llama" => 3`
*/

use std::collections::HashMap;
#[allow(dead_code)]
pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn first_uniq_char(s: String) -> i32 {
        let mut counts: HashMap<char, i32> = HashMap::new();
        for c in s.chars() {
            match counts.get(&c) {
                Some(x) => counts.insert(c, x + 1),
                None => counts.insert(c, 1),
            };
        }

        for (idx, c) in s.chars().enumerate() {
            match counts.get(&c) {
                Some(x) => match x {
                    1 => return i32::try_from(idx).expect("Conversion error!"),
                    _ => continue,
                },
                None => continue,
            }
        }

        return -1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_first_letter() {
        assert_eq!(Solution::first_uniq_char("abcd".to_string()), 0);
    }
    
    #[test]
    fn test_first_not_unique() {
        assert_eq!(Solution::first_uniq_char("llama".to_string()), 3);
    }

    #[test]
    fn test_no_unique_chars() {
        assert_eq!(Solution::first_uniq_char("abab".to_string()), -1);
    }
}
