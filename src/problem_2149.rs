/*!
 * Problem 2149
 * POTD Feb 14, 2024
 */

pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn rearrange_array(nums: Vec<i32>) -> Vec<i32> {
        let mut result = Vec::with_capacity(nums.len());

        let mut neg = Vec::with_capacity(nums.len() / 2);
        let mut pos = Vec::with_capacity(nums.len() / 2);

        for val in nums.iter() {
            if *val < 0 {
                neg.push(*val);
            } else {
                pos.push(*val);
            }
        }

        // Both arrays are guaranteed to be the same size
        for i in 0..neg.len() {
            result.push(pos[i]);
            result.push(neg[i]);
        }

        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rearrange_two_nums() {
        let input = vec![-20, 30];
        assert_eq!(Solution::rearrange_array(input), vec![30, -20])
    }

    #[test]
    fn test_empty() {
        let input = vec![];
        assert_eq!(Solution::rearrange_array(input), vec![]);

    }

    #[test]
    fn test_in_order() {
        let input = vec![1,2,3,-1,-2,-3];
        assert_eq!(Solution::rearrange_array(input), vec![1,-1,2,-2,3,-3]);
    }
}