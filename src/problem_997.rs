/*!
 Problem 997
 Feb 22, 2024

 There are `n` people numbered `1` through `n`. Exactly one MAY be a "town judge".
 If that person exists, then

 1) Everybody trusts them
 2) They trust nobody

 Identify the judge given a `Vec<Vec<i32>>` describing the trust relationships.
 For example, `[1, 2]` as an element of the outer `Vec` implies that person
 1 trusts person 2 (the relationship is NOT symmetric)

 If no judge exists, return `-1`
 */
pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn find_judge(n: i32, trust: Vec<Vec<i32>>) -> i32 {
        let size = n.try_into().unwrap();
        let mut num_trusted_by = vec![0; size + 1];
        let mut num_who_trust = vec![0; size + 1];

        for pair in trust { //pair[0] trusts pair[1]
            num_trusted_by[pair[0] as usize] += 1;
            num_who_trust[pair[1] as usize] += 1;
        }

        // Try to find the judge
        for i in 1..=size {
            if num_who_trust[i] == size - 1 && num_trusted_by[i] == 0 {
                return i as i32;
            }
        }
        -1
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn two_people() {
        let trust = vec![vec![1, 2]];
        assert_eq!(Solution::find_judge(2, trust), 2);
    }

    #[test]
    fn two_people_trusting() {
        let trust = vec![vec![1, 2], vec![2, 1]];
        assert_eq!(Solution::find_judge(2, trust), -1);
    }

    #[test]
    fn no_trust() {
        let trust = vec![];
        assert_eq!(Solution::find_judge(10, trust), -1);
    }
}

