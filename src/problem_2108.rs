/*!
 * Problem 2108
 * Feb 13
 * 
 * Given a `Vec<String>`, return the first `String` in the vector which is
 * a palindrome.. If there are none, return `""`
 */

use std::sync::mpsc;
use std::thread;

 #[allow(dead_code)]
pub struct Solution {}

impl Solution {
    fn is_palindrome(s: &String) -> bool {
        if s.len() == 0 { // s.len() - 1 will overflow
            return true;
        }

        let (mut left, mut right) = (0usize, s.len() - 1);
        let chars: Vec<char> = s.chars().collect();
        while left < right {
            if chars[left] != chars[right] {
                return false;
            }
            left += 1;
            right -= 1;
        }
        return true;
    }

    #[allow(dead_code)]
    pub fn first_palindrome(words: Vec<String>) -> String {
        for s in words.iter() {
            if Solution::is_palindrome(s) {
                return s.clone();
            }
        }
        String::from("")
    }

    #[allow(dead_code)]
    pub fn first_palindrome_threaded(words: Vec<String>) -> String {
        let (tx, rx) = mpsc::channel();
        let mut i = 0;
        while i < words.len() {
            let input = words[i].clone();
            let tx_clone = tx.clone();
            let _handle = thread::spawn(move || {
                let result = Solution::is_palindrome(&input);
                tx_clone.send( (i, result) ).unwrap();
            });
            i += 1;
        }

        let mut received = 0usize;
        let mut earliest = -1i32;
        let mut found = false;
        for (idx, result) in rx {
            println!("{} {}", idx, result);
            if result && (earliest == -1 || idx < earliest as usize) {
                found = true;
                earliest = idx as i32;
            }
            received += 1;
            if received >= words.len() {
                break;
            }
        }
        return if found { words[earliest as usize].clone() } else { String::from("") };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_palindrome() {
        assert_eq!(Solution::is_palindrome(&String::from("racecar")), true);
    }

    #[test]
    fn test_not_palindrome() {
        assert_eq!(Solution::is_palindrome(&String::from("abab")), false);
    }

    #[test]
    fn test_empty_palindrome() {
        assert_eq!(Solution::is_palindrome(&String::from("")), true);
    }

    #[test]
    fn test_first_palindrome_in_list() {
        let input = vec![String::from("racecar"), String::from("cat")];
        assert_eq!(Solution::first_palindrome(input), String::from("racecar"));
    }

    #[test]
    fn test_last_palindrome_in_list() {
        let input = vec![String::from("cat"), String::from("racecar")];
        assert_eq!(Solution::first_palindrome(input), String::from("racecar"));
    }

    #[test]
    fn test_no_palindrome_in_list() {
        let input = vec![String::from("cat"), String::from("car")];
        assert_eq!(Solution::first_palindrome(input), String::from(""));
    }

    #[test]
    fn test_threaded() {
        let input = vec![String::from("cat"), String::from("car")];
        assert_eq!(Solution::first_palindrome_threaded(input), String::from(""));

    }
}