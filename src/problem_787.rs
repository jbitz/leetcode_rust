/*!
Problem 787
February 23, 2024

https://leetcode.com/problems/cheapest-flights-within-k-stops/description/
 */

pub struct Solution {}

use std::collections::{HashMap, LinkedList};

impl Solution {
    #[allow(unused_variables)]
    pub fn find_cheapest_price(n: i32, flights: Vec<Vec<i32>>, src: i32, dst: i32, k: i32) -> i32 {
        let mut costs: HashMap<i32, HashMap<i32, i32>> = HashMap::new();
        
        // Build a map start -> (finish -> cost)
        for flight in flights {
            let from = flight[0];
            let to = flight[1];
            let cost = flight[2];

            if let Some(neighbors) = costs.get_mut(&from) {
                neighbors.insert(to, cost);
            } else {
                let mut new_map = HashMap::new();
                new_map.insert(to, cost);
                costs.insert(from, new_map);
            }
        }
        println!("{:?}", costs);

        // BFS to find cheapest path with at most k hops
        let mut q = LinkedList::new();
        // (cur, distance)
        q.push_front((src, 0));
        let mut best_costs = HashMap::new();

        let mut stops = 0;
        while stops <= k {
            let size = q.len();
            for _ in 0..size {
                let (cur, cost) = q.pop_front().unwrap();
                println!("{}, {}", cur, cost);
                if let Some(neighbors) = costs.get(&cur) {
                    for (neighbor, path_cost) in neighbors {
                        let new_cost = cost + path_cost;

                        if let Some(val) = best_costs.get_mut(neighbor) {
                            if new_cost < *val {
                                best_costs.insert(*neighbor, new_cost);
                                q.push_back((*neighbor, new_cost))
                            }
                        } else {
                            best_costs.insert(*neighbor, new_cost);
                            q.push_back((*neighbor, new_cost))
                        }
                    }
                }
            }
            stops += 1;
        }

        match best_costs.get(&dst) {
            Some(c) => *c,
            None => -1
        }        
        
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_small_case() {
        let flights = vec![
            vec![0,1,100],
            vec![1,2,100],
            vec![2,0,100],
            vec![1,3,600],
            vec![2,3,200]
        ];
        assert_eq!(Solution::find_cheapest_price(4, flights, 0, 3, 1), 700);
    }
}