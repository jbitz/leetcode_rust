/*!
 Problem 2971
 February 15, 2024
  
 Given a `Vec<i32>` of `n` positive integers, return the largest possible perimeter
 of a polygon with a subset of those integers as its side length
  
 Theorem: Given `k <= 3` positive real numbers `a_1, a_2, ...` where
 `a_1 <= a_2 <= a_3 <= ...` and `a_1 + ... + a_{k-1} > a_k`, then a polygon
 with those side lengths exists
 */

pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn largest_perimeter(nums:Vec<i32>) -> i64 {
        let mut sorted: Vec<i64> = nums.into_iter().map(|x| x as i64).collect();
        sorted.sort();

        let mut best_perimeter = -1;
        let mut cur_sum = sorted[0] + sorted[1];
        for val in sorted[2..].iter() {
            if cur_sum > *val { // This is a valid polygon
                best_perimeter = std::cmp::max(best_perimeter, cur_sum + *val);
            }
            cur_sum += *val;
        }

        best_perimeter   
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn regular_polygon() {
        let input = vec![5, 5, 5];
        assert_eq!(Solution::largest_perimeter(input), 15);
    }

    #[test]
    fn some_too_long() {
        let input = vec![1,12,1,2,5,50,3];
        assert_eq!(Solution::largest_perimeter(input), 12);
    }

    #[test]
    fn cannot_make_polygon() {
        let input = vec![1,1,50];
        assert_eq!(Solution::largest_perimeter(input), -1);
    }
}