/*!
 Problem 647
 Feb 10, 2024

 Given a string `s`, return the number of **palindromic substrings** in it.
 */


pub struct Solution {}

impl Solution {

    #[allow(dead_code)]
    pub fn count_substrings(s: String) -> i32 {
        let st = s.as_bytes();

        let mut dp = vec![vec![false; s.len()]; s.len()];

        let mut count = 0;
        for i in 0..s.len() {
            dp[i][i] = true; // Single letter strings - all palindromes
            count += 1;
            if i + 1 < s.len() && st[i] == st[i+1] { // Two letter strings
                dp[i][i+1] = true;
                count += 1;
            }
        }

        // x = start index, y = end index
        for y_offset in 2..s.len() {
            for x in 0..s.len() - y_offset {
                dp[x][x+y_offset] = (st[x] == st[x+y_offset]) && dp[x + 1][x+y_offset - 1];
                if dp[x][x+y_offset] {
                    count += 1;
                }
            }
        }
        count
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_all_same() {
        assert_eq!(Solution::count_substrings(String::from("aaa")), 6);
    }

    #[test]
    fn test_no_palindromes() {
        assert_eq!(Solution::count_substrings(String::from("abc")), 3);
    }

    #[test]
    fn test_total_palindrome() {
        // "racecar", "aceca", "cec", then all the single-letter ones
        assert_eq!(Solution::count_substrings(String::from("racecar")), 10);
    }
}