/*!
 Problem 1642
 Feb 17, 2024
  
  `heights` is a vector of building heights. To climb each building, you can
  either build a tower of (multible) bricks or use a single ladder. You can go from a
  taller building to a shorter one with zero cost.

  Return the furthest building index that you can reach under these rules
 */

use std::collections::BinaryHeap;
use std::cmp::Reverse;

pub struct Solution {}

impl Solution {
    #[allow(dead_code)]
    pub fn furthest_building(heights: Vec<i32>, bricks: i32, ladders: i32) -> i32 {
        let mut bricks_remaining = bricks;

        // Make sure that the `ladders` highest values are covered with ladders
        // Lowest after that get covered with bricks
        // Keep a PQ of height differences we've seen so far
        let mut tallest_heights = BinaryHeap::new();
        
        for idx in 0..heights.len() - 1 {
            let d_height = heights[idx + 1] - heights[idx];
            if d_height <= 0 {
                continue;
            }

            tallest_heights.push(Reverse(d_height));
            if tallest_heights.len() > (ladders as usize) {
                let Reverse(dh) = tallest_heights.pop().expect("Should not be empty!");
                if bricks_remaining >= dh {
                    bricks_remaining -= dh;
                } else { // Can't go any further
                    return idx as i32;
                }
            }
        }

        (heights.len() - 1) as i32
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_no_bricks_ladders() {
        let heights = vec![1,2,3,4,5];
        let bricks = 0;
        let ladders = 0;
        let ans = 0;
        assert_eq!(Solution::furthest_building(heights, bricks, ladders), ans)
    }

    #[test]
    fn test_only_ladders() {
        let heights = vec![1,2,3,4,5];
        let bricks = 0;
        let ladders = 2;
        let ans = 2;
        assert_eq!(Solution::furthest_building(heights, bricks, ladders), ans)
    }

    #[test]
    fn test_can_descend() {
        let heights = vec![4,3,2];
        let bricks = 0;
        let ladders = 0;
        let ans = 2;
        assert_eq!(Solution::furthest_building(heights, bricks, ladders), ans)
    }

    #[test]
    fn test_brick_then_ladder() {
        let heights = vec![1,4,10,20];
        let bricks = 4;
        let ladders = 1;
        let ans = 2;
        assert_eq!(Solution::furthest_building(heights, bricks, ladders), ans)
    }

    #[test]
    fn test_ladder_then_brick() {
        let heights = vec![1,10,4,20];
        let bricks = 4;
        let ladders = 1;
        let ans = 2;
        assert_eq!(Solution::furthest_building(heights, bricks, ladders), ans)
    }
}