
mod problem_787;
fn main() {
    let flights = vec![
            vec![0,1,100],
            vec![1,2,100],
            vec![2,0,100],
            vec![1,3,600],
            vec![2,3,200]
        ];
    assert_eq!(problem_787::Solution::find_cheapest_price(4, flights, 0, 3, 1), 700);
    println!("You should run cargo test instead");
}
